using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Stats : MonoBehaviour {

    public static Stats Singleton;
    public int Health = 100;
    public int Energy = 100;
    public int Happiness = 100;
    public Pet CurrentPet;
    public Pet[] PetStatus;
    public float Timeline = 1;
    int TimeElapsed;
    public int HungerRate = 10;
    int HungerTime;
    public Slider SLDHealth;
    public Slider SLDEnergy;
    public Slider SLDHappiness;
    int ActualState = 0;
    public int ChangeoverTime = 25;
    public Button BTNfeed;
    public int HappinessRate= 5;
    int HappinessTime;
    /*
      -1 : died
      0 : initial
     ...
     */

    private void Awake()
    {
        if (Singleton == null)
        {
            Singleton = this;
        }
        else
        {
            Destroy(this);
        }
    }
    // Use this for initialization
    void Start ()
    {
        SetUp();
        BTNfeed.onClick.AddListener(feed);
        CurrentPet = Instantiate(PetStatus[1], Vector3.zero, Quaternion.identity);
        InvokeRepeating("Updating", 0, Timeline);
	}

    void SetUp()
    {
        SLDHealth.value = Health;
        SLDEnergy.value = Energy;
        SLDHappiness.value = Happiness;
    }
	// Update is called once per frame
	void Update () {
		
	}

    void feed()
    {
        if (Energy < 100)
        {
            if (Happiness > 20)
            {
                Energy += 20;
            }
        }
        else
        {
            Energy = 100;
        }
    }

    void Updating()
    {
        if (ActualState > 0)
        {
            if (HappinessRate < HappinessTime)
            {
                HappinessTime = 0;
                HappinessTime--;
                SLDHappiness.value = Happiness;
            }
            HappinessTime++;

            if (HungerTime > HungerRate)
            {
                HungerTime = 0;
                Energy--;


                if (Energy <= 50)
                {
                    Health--;
                    SLDHealth.value = Health;
                    if (Health <= 0)
                    {
                        died();
                    }
                }
                else
                {
                    if (Health < 100)
                    {
                        Health++;
                        SLDHealth.value = Health;
                    }
                }
                SLDEnergy.value = Energy;
            }
            HungerTime++;
        }
        if ((ActualState > -1) || ActualState > PetStatus.Length - 2)
        { 
            if (TimeElapsed % ChangeoverTime == 0)
            {
                ChangeStatus();
            }
        }

        TimeElapsed++;
    }

    void died()
    {
        Destroy(CurrentPet.gameObject);
        CurrentPet = Instantiate(PetStatus[0], Vector3.zero, Quaternion.identity);
        ActualState = -1;
    }

    void ChangeStatus()
    {
        int s = TimeElapsed / ChangeoverTime;
        if (s == ActualState)
        {
            return;
        }
        else
        {
            Destroy(CurrentPet.gameObject);
            CurrentPet = Instantiate(PetStatus[s+1], Vector3.zero, Quaternion.identity);
            ActualState = s;
        }
    }
}
