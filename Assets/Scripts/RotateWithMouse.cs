using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RotateWithMouse : MonoBehaviour {
    float rotSpeed = 120f;
    public Text hora;
    int contador = 0;

    private void Awake()
    {
        contador = 0;
    }
    private void OnMouseDrag()
    {
        float rotX = Input.GetAxis("Mouse Y") * rotSpeed * Mathf.Deg2Rad;
        transform.Rotate(Vector3.right, rotX);
        contador ++;
        if (Input.GetAxis("Mouse Y") > 0)
        {
            //texto suba
            print("dragueando hacia arriba!!!!!!");
        }
        else if (Input.GetAxis("Mouse Y") < 0)
        {
            //texto baje
            print("dragueando hacia Abajo -----");
        }
        else
        {
            print("Sin drag");
        }
    }
  
}
