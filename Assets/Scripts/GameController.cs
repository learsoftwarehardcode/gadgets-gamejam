﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {
	public int[] nivelesSpeeds = new int[10];//array con los modificadores de rotación por nivel //10 niveles	

	[Range (1,10)]
	public int curNivel = 1;//nivel actual

	public int lastNivel = 0; //last nivel

	// Use this for initialization
	void Start () {
		this.fillNivelesSpeeds (nivelesSpeeds);
	}
	
	// Update is called once per frame
	void Update () {
		//prueba de cambio de mierda al código.
	}

	/**
	 * Carga los niveles de velocidades
	 **/
	public void fillNivelesSpeeds(int[] arraySpeeds)
	{
		int start = 15;
		int delta = 10;
		for (int i = 0; i < arraySpeeds.Length; i++) {
			arraySpeeds [i] = start + i * delta;
		}
	}
}
