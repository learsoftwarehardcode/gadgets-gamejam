﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour {
	public GameController GAME = null;
	public int lastNivel = 0;

	[ContextMenuItem("Reestablecer", "resetGiro")]
	public int modificadorGiro;
	public void resetGiro() {
		modificadorGiro = 0;
	}	

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update()
	{
		if (GAME == null) //MALOOO CORREGIR
			GAME = (GameController)GameObject.Find("GameController").GetComponent("GameController");

		transform.Rotate(Vector3.forward * modificadorGiro * Time.deltaTime);
		this.checkAndApplyLevelChange ();

	}


	public void checkAndApplyLevelChange(){
		
		if (GAME.curNivel != this.lastNivel) {
			this.modificadorGiro = GAME.nivelesSpeeds[GAME.curNivel];
			this.lastNivel = GAME.curNivel;
		}
	}
}
